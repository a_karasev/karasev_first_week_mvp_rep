package repositories;

import models.Credit;

public interface CreditRepository {

    void addCredit(Credit credit);
    void removeCredit(Credit credit);
    void updateCredit(Credit credit);



}
