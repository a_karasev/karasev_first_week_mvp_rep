package presenters;

import java.util.Date;

public interface CreditInput {

    void input(long number, Date date, double suum);
}
