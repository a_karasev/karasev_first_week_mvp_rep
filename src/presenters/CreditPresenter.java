package presenters;

import models.Credit;
import views.CreditView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreditPresenter implements CreditInput {
    private Credit credit;
    private List<CreditView> creditViews = new ArrayList<>();

    public void setCredit(Credit credit){
        this.credit = credit;
    }

    public void addCreditView(CreditView creditView){
        creditView.addCreditInput(this);
        this.creditViews.add(creditView);
    }

    public void updateCreditView(){
        for(CreditView creditView : creditViews){
            creditView.uptdateCredit(credit.getNumber(), credit.getDate(), credit.getSumm());
        }
    }

    @Override
    public void input(long number, Date date, double summ) {
        this.credit.setNumber(number);
        this.credit.setDate(date);
        this.credit.setSumm(summ);
        this.updateCreditView();
    }
}
