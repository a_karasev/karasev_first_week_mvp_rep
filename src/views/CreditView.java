package views;

import presenters.CreditInput;

import java.util.Date;

public interface CreditView {

    void uptdateCredit(long number, Date date, double summ);
    void view();
    void addCreditInput(CreditInput creditInput);
    void input(long number, Date date, double summ);
}
