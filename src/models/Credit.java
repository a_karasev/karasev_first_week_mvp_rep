package models;

import java.util.Date;

public class Credit {
    private long number;
    private Date date;
    private double summ;

    public Credit(){

    }

    public Credit(long number, Date date, double summ) {
        this.number = number;
        this.date = date;
        this.summ = summ;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getSumm() {
        return summ;
    }

    public void setSumm(double summ) {
        this.summ = summ;
    }


}
